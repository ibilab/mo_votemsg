package net.infobank.itv.mo_votemsg.util;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_votemsg.dao.ScheduleDAO;
import net.infobank.itv.mo_votemsg.dao.VoteMsgDAO;
import net.infobank.itv.mo_votemsg.model.VoteMsg;
import net.infobank.itv.mo_votemsg.model.ReplyMt;
import net.infobank.itv.mo_votemsg.model.VoteData;

public class VoteUtils {
    private static Logger log = LoggerFactory.getLogger(VoteUtils.class);
    

    public static List<VoteMsg> getVoteMsgList(VoteMsg vote ) {
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	List<VoteMsg> list = new ArrayList<VoteMsg>();
        try {
        	list = voteDAO.selectVoteMsgList(vote);
        	
        	for(int i = 0; i < list.size() ; i++) {
        		
        		log.info("getVote_server_id : " + vote.getVote_server_id()  );
        		
	            if(vote.getVote_server_id() == 1) {
	            	list.get(i).setStr_vote_server_id("mac_status");
	            }
	            else {
	            	list.get(i).setStr_vote_server_id("mac_status_" + vote.getVote_server_id());
	            }
	            log.info("getStr_vote_server_id : " + list.get(i).getStr_vote_server_id()  );
        	}
            
        } catch (Exception e) {
            log.error("Exception getVoteMsgList : ", e);
        }
        
        return list;
    }
    
    
    public static int  getVoteUserDupCheck(int dup , ProgramMsg msg) {
        
    	int ret = 0; 
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        
        try {
            ret = voteDAO.getVoteUserDupCheck(dup, msg);
        } catch (Exception e) {
            log.error("Exception getVoteOneUserCount : ", e);
        }
        return ret;
    }
    
    public static void updateMacStatus(int status, VoteMsg vote) {
        
    	 
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	vote.setMac_status(status);
        
        try {
        	
        	log.info("updateMacStatus : " + vote.toString());		
            voteDAO.updateMacStatus(vote);
        } catch (Exception e) {
            log.error("Exception updateMacStatus : ", e);
        }
        
    }
    
    
    public static int getMacStatus(VoteMsg vote) {
    	
    	int status = 0;
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	

    	
        try {
        	status = voteDAO.getMacStatus(vote);
        	
        	
        	if(status == 2 ) {
                // 투표 끝 시간 체크 
            	Calendar time = Calendar.getInstance();
                SimpleDateFormat datetime = new SimpleDateFormat("yyyyMMddHHmmss");
                String current_time = datetime.format(time.getTime());
                
                if ( vote.getVote_etm().compareTo(current_time) < 0  ) {
                	log.info("투표 종료 시간을 넘었습니다.[" +  current_time +  "] > [" + vote.getVote_etm() + "]" );
                	updateMacStatus(3, vote); 
                	
                	status = 3; 
                }
        	}
        	
        } catch (Exception e) {
            log.error("Exception getVoteStatus : ", e);
        }
        
        return status;
    }
    
    public static int getVote_RsltStatus(VoteMsg vote) {
    	
    	int status = 0;
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
    	
        try {
        	status = voteDAO.getMacStatus(vote);
        	
        } catch (Exception e) {
            log.error("Exception getMacStatus : ", e);
        }
        
        return status;
    }
    

    

    public static ReplyMt selectBillingCode (int pgm_key) {
    	ReplyMt replymt = null;
    	VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        
        try {
        	replymt = voteDAO.selectBillingCode(pgm_key);
        } catch (Exception e) {
            log.error("Exception getAllowUser : ", e);
        }
        return replymt;
    }
    
    public static int mtBulkInsert_2(List<VoteData> list) {
        int id = -1;
        VoteMsgDAO voteDAO = new VoteMsgDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = voteDAO.mtBulkInsert_2(list);
        } catch(Exception e)  {
            log.error("mtbulkinsert_2 : " , e);            
        }

        return id;
    }
    
}
