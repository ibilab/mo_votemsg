package net.infobank.itv.mo_votemsg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import net.infobank.itv.mo_common.model.ConsumerInfo;
//import net.infobank.itv.mo_common.model.Vote;
import net.infobank.itv.mo_common.util.CommonUtils;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_votemsg.kafka.ConsumerThread;
import net.infobank.itv.mo_votemsg.model.VoteMsg;
import net.infobank.itv.mo_votemsg.model.VoteKeyword;
import net.infobank.itv.mo_votemsg.util.VoteUtils;

@SpringBootApplication
public class Mo_VoteMsg {
    private static Logger log = LoggerFactory.getLogger(Mo_VoteMsg.class);

    public HashMap<String, Object> thread_list;
    public ConsumerInfo conInfo;

    public static void main(String[] args) throws InterruptedException {
        Mo_VoteMsg pgmAssign = new Mo_VoteMsg();
        pgmAssign.start(args);
    }

    public void start(String[] args) throws InterruptedException {
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
        FileConfig _config;

        // 접속정보 를 파일에서 가져온다.
        if (args.length != 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            return;
        }

        _config = new FileConfig(args[0]);

        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        // Queue file directory 만들기
        CommonUtils.makeDir(_config.getMo_qdir());
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);

        conInfo = new ConsumerInfo();
        conInfo.setBoot_strap_server(_config.getKafkaBroker());
        conInfo.setPre_group_name(group_name);
        String strvote_server_id = _config.getVote_server_id(); 
        int vote_server_id = Integer.parseInt(strvote_server_id);
        conInfo.setVote_server_id(vote_server_id);
        conInfo.setRedis_svr_1(_config.getRedis_svr_1());
        
        String str_redis_port = _config.getRedis_port_1(); 
        int redis_port = Integer.parseInt(str_redis_port);
        conInfo.setRedis_port_1(redis_port);
        

        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version); 
        
        log.info("버전: " + nVersion);
        log.info("vote_server_id" + vote_server_id);
        
        while (true) { 
        	
        	VoteMsg vote = new VoteMsg();
        	vote.setVote_server_id(vote_server_id);
        	
        	List<VoteMsg> list = new ArrayList<VoteMsg>();
        	
        	
        	list = VoteUtils.getVoteMsgList(vote);
        	
        	log.info("투표메시지 시작 카운트는 : " + list.size());
        	
        	for(int i = 0; i < list.size() ; i++) {
        		log.info("투표 메시지 정보 : " + list.get(i).toString());
            		votemsgConsumerThread(list.get(i));
        	}
	        Thread.sleep(5000);
        }
    }
    
    public ArrayList<String> diffArray(ArrayList<String> arr1, ArrayList<String> arr2) {
        ArrayList<String> list1 = new ArrayList<String>(arr1);
        ArrayList<String> list2 = new ArrayList<String>(arr2);
        list1.removeAll(list2);
        return list1;
    }
    
    public void votemsgConsumerThread(VoteMsg vote) {
        //conInfo.setVote_key(vote_key);

    	// 집계
        Runnable consumer = new ConsumerThread(conInfo, vote);
        Thread thread_consumer = new Thread(consumer);
        thread_consumer.start();
        
    }
    
  

    public class ShutdownThread extends Thread {
        public void run() {
            for (int i = 0; i < thread_list.size() ; i++) {
                ConsumerThread thread = (ConsumerThread) thread_list.get(i+"");
                thread.shutdown();
            }
        }
    }
}
