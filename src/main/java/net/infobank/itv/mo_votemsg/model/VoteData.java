package net.infobank.itv.mo_votemsg.model;
import java.time.LocalDateTime;

import lombok.Data;


@Data
public class VoteData {
    int  vote_key;
    String vote_user;
    int  vote_num;
    String  vote_regdate;
    int server_id;
    String msg_com;
    String msg_key;
    String msg_cnet;
    String msg_type;
    String subject;
    String mt_msg;
    String ref_key;
    int ch_key;
    int pgm_key;
    String msg_callback;
}
