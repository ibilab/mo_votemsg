package net.infobank.itv.mo_votemsg.model;

import lombok.Data;


@Data
public class VoteKeyword {
    int     num;      
    String    item;
    String    keyword;
    String    sujebct;
    String    mt_msg;
}
