package net.infobank.itv.mo_votemsg.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class VoteMsg implements Serializable {


	private int pgm_key;
	private String pgm_mo;
	private String pgm_emo;
	private String msg_type;
	private int vote_key;
	private int vote_server_id;
	private String str_vote_server_id;
	private String vote_regdate;
	private String vote_stm;
	private String vote_etm;
	private int vote_status;
	private int vote_status_2;
	private int mac_status;
	private int mac_status_2;
	private String vote_title;
	private int vote_count;
	private String vote_items;
	private String vote_keywords;
	private int vote_user_dup;
	private int vote_emo;
	private int vote_keyword_dup;
	private int sub_key;
	private int sns_count;
	private String sns_list;
	private String mac_subjects;
	private String mac_msgs;

}

