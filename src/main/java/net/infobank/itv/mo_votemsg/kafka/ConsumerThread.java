package net.infobank.itv.mo_votemsg.kafka;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.infobank.itv.mo_common.model.ConsumerInfo;
import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.util.CommonUtils;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_votemsg.model.VoteMsg;
import net.infobank.itv.mo_votemsg.model.ReplyMt;
import net.infobank.itv.mo_votemsg.model.VoteData;
import net.infobank.itv.mo_votemsg.model.VoteKey;
import net.infobank.itv.mo_votemsg.model.VoteKeyword;
import net.infobank.itv.mo_votemsg.util.VoteUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;



public class ConsumerThread implements Runnable {
    private static Logger log = LoggerFactory.getLogger(ConsumerThread.class);

    String BootStrapServer;
    ArrayList<String> Monum = new ArrayList<String>();
    String Pre_Groupname;
    String Web_Root;
    String Web_Dir;
    String Redis_Server;
    int Redis_Port;
    int RefreshTimer;
    int ReLoad_Sec;
    static String Mo_QDir;
    static String Db_Name;
    int vote_key;
    int vote_server_id;
    VoteMsg vote_info; 
    List<VoteKeyword> votekeywords = new ArrayList<VoteKeyword>();
    List<ProgramMsg> programmsg = new ArrayList<ProgramMsg>();
    List<VoteData> votedata_list = new ArrayList<VoteData>();
    List<VoteKey> votemsg_list = new ArrayList<VoteKey>();
    JedisPool jedispool;
    Jedis jedis = null;

    		
    
    // Sub Consumer
    KafkaConsumer<String, String> consumer = null;
    private Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

    private final AtomicBoolean running = new AtomicBoolean(false);

    public ConsumerThread(ConsumerInfo Info, VoteMsg vote) {
        // store parameter for later user
        BootStrapServer = Info.getBoot_strap_server();
        //Monum = Info.getMo_num();
        Pre_Groupname = Info.getPre_group_name();
        ReLoad_Sec = Info.getReload_sec();
        Web_Root = Info.getWeb_root();
        Web_Dir = Info.getWeb_dir();
        Mo_QDir = Info.getMo_QDir();
        Db_Name = Info.getDb_name();
        vote_key = Info.getVote_key();
        vote_server_id = Info.getVote_server_id();
        vote_info = vote;
        Redis_Server = Info.getRedis_svr_1();
        Redis_Port = Info.getRedis_port_1();
        
    }

    public void shutdown() {
        log.info("Stop this thread");
        running.set(false);
    }

    private class HandleRebalance implements ConsumerRebalanceListener {
        public void onPartitionsAssigned(Collection<TopicPartition> partitions) { 
        }

    	public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
    	    log.info("Lost partitions in rebalance. " +
    	            "Committing current offsets:" + currentOffsets);
    	    consumer.commitSync(currentOffsets); 
    	    
    	    // Reblanacing error add 
    	    currentOffsets.clear(); // 추가 코드
	    }
    }
    
	@Override
    public void run() {
        Thread shutDownHook = new ShutdownHook(Thread.currentThread(), "Shutdown");
        Runtime.getRuntime().addShutdownHook(shutDownHook);
        
        log.info("run thread : " + Thread.currentThread().getName());
        log.info("run vote_info : " + vote_info.toString());

        String group;
        String message = null;
        Gson g = new Gson();
        int gapTime = 0;
        int currentTime = 0;
        List<ProgramMsg> programMsg_list = new ArrayList<ProgramMsg>();

        // 쓰레드 스탑
        running.set(true);

        // sUB sETTING
        group = Pre_Groupname;
        log.info("groupname : " + group);

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootStrapServer);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        props.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, false);
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        
        // from begining 
        //props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumer = new KafkaConsumer<String, String>(props);
        
        // 토픽명 생성 
        String topic = vote_info.getPgm_mo().replace("#", "") + vote_info.getPgm_emo()  + "_pgm";
        log.info("topic name : " + topic);
        Monum.add(topic);
        
        // init vote_data & vote_rslt
        //VoteUtils.initVoteData(vote_info);
        
        // 투표 status 2로 변경
        VoteUtils.updateMacStatus(2, vote_info);
        
        // 투표 키워드 및 번호 분리 
        votekeywords = Divide_Item(vote_info.getVote_count(),  vote_info.getVote_items(), vote_info.getVote_keywords(), vote_info.getMac_subjects(), vote_info.getMac_msgs());
        log.info(votekeywords.size() + "---- " + votekeywords.toString() );
        
        // 투표키워드 길이로 sorting 
        LengthComparator comp = new LengthComparator();
        Collections.sort(votekeywords, comp);
        

        // REDIS  접속 .... 
        if(vote_info.getVote_user_dup() == 1 || vote_info.getVote_user_dup() == 2) {
        	
        	try {
	        	JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
	        	  
	        	log.info("Redis Server Connect Server " + Redis_Server + ":" + Redis_Port);
	        	jedispool = new JedisPool(jedisPoolConfig, Redis_Server, Redis_Port, 1000);
	        	jedis = jedispool.getResource();
        	
            } catch(Exception e) {
                log.error("Exception : ", e);
            }
        }
                
        try {
        	consumer.subscribe(Monum, new HandleRebalance());
        	
         	while (running.get()) {
              	
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, String> record : records) {
                    try {
                        message = record.value();
                        
                        log.info("topic = {}, partition = {}, offset = {}, key = {}, msg = {}",
                                record.topic(), record.partition(), record.offset(),
                                record.key(), record.value());
                        
                        // Msg JSon 변환
                        JsonReader reader_body = new JsonReader(new StringReader(message));
                        reader_body.setLenient(true);
                        ProgramMsg body = g.fromJson(reader_body, ProgramMsg.class);
                        
                        programmsg.add(body);
                        
                    } catch(Exception e) {
                        log.error("Exception : ", e);
                    }
                }
                
                // 집꼐 로직 
                if(programmsg.size() > 0) {
                	votedata_list = proc_vote(vote_info, programmsg);
                	programmsg.clear();
                	
                	// bulk insert 해라 
                    if(votedata_list.size() > 0) {  
                  	  
                    	
                    	int count = VoteUtils.mtBulkInsert_2(votedata_list);
                    	log.info("투표 메시지 데이터 inserrt 갯수 : "  + votedata_list.size() + ", ret 갯수 : ", count);
                    	votedata_list.clear();
                    }
                }
                
                // 투표 상테 체크 
                int status = VoteUtils.getMacStatus(vote_info);
                if(status == 3) {
                	log.info("투표  상태가 종료 되었습니다 : " );
                	Stop_Vote(vote_info);
                }
                else {
                	log.info("투표  상태 : " + status );
                }
                               
                //list vote_data insert 
                consumer.commitAsync(currentOffsets, null);
        	} 
 
        } catch (WakeupException e) {
            // ignore, we're closing
        } catch(Exception e) {
            log.error(message, e);
        } finally {
            try {
                consumer.commitSync(currentOffsets);
            } finally {
                consumer.close();
                if(jedispool != null )
                	jedispool.close();
    	        log.info("Exit 투표 thread : " + Thread.currentThread().getName());
            }
        }
    }
	
    public List<VoteKeyword> Divide_Item(int count, String Items, String Keywords, String Subjects, String Mtmsgs) {
    	
    	List<VoteKeyword> votekeyword_list = new ArrayList<VoteKeyword>();
    	List<VoteKey> votekey_list = new ArrayList<VoteKey>();
    	
        try {
        	
            String Item = Items.replaceAll("\\s+$","");
            String Keyword = Keywords.replaceAll("\\s+$","");
            
            String Subject = Subjects.replaceAll("\\s+$","");
            String Mtmsg = Mtmsgs.replaceAll("\\s+$","");
            
            StringTokenizer Item_token = new StringTokenizer(Item, "|");
            StringTokenizer Subject_token = new StringTokenizer(Subject, "|");
            StringTokenizer Mtmsg_token = new StringTokenizer(Mtmsg, "|");
            
            
            StringTokenizer Keyword_token = new StringTokenizer(Keyword, "|");
        	
        	
        	if(Item_token.countTokens() != count || Keyword_token.countTokens() != count) {
        		
        		log.error(" " + count + " " + Item_token.countTokens() + " " + Keyword_token.countTokens() );
        		log.error("keyword count is not equal tokent count " + Keywords, count);
        		return null;
        	}        	
        	
        	if(Subject_token.countTokens() != count || Mtmsg_token.countTokens() != count) {
        		
        		log.error(" " + count + " " + Subject_token.countTokens() + " " + Mtmsg_token.countTokens() );
        		log.error("Subject or Mtmsg count is not equal tokent count " + Subjects, count);
        		return null;
        	}  
        	
        	Item_token.hasMoreTokens();
        	Subject_token.hasMoreTokens();
        	Mtmsg_token.hasMoreTokens();
        	Keyword_token.hasMoreTokens();
        	
        	for (int i = 0 ; i < count ; i++)  {
        		VoteKey votekey = new VoteKey();
        		VoteKeyword votekeyword = new VoteKeyword();
        		
        		votekey.setItem(Item_token.nextToken().toUpperCase());
        		votekey.setSujebct(Subject_token.nextToken());
        		votekey.setMt_msg(Mtmsg_token.nextToken());
        		votekey.setKeyword(Keyword_token.nextToken().toUpperCase());
        		
        		votekey.setNum(i+1);
        		
        		votekey_list.add(votekey);
        		
        		//mt msg 
        		votemsg_list.add(votekey);
        		
        		log.info("Item 분리 " + votekey_list.toString());
        	}
        	
    		log.info("keyword 분리 시작 " + votekey_list.toString());
    		votekeyword_list = Divide_Keyword(votekey_list);
    		
        
        } catch(Exception e) {
            log.error(Keywords, e.getStackTrace(), e);
        }
        
		return votekeyword_list;
		
    }
    
    public List<VoteKeyword> Divide_Keyword(List<VoteKey> votekey_list ) {
    	List<VoteKeyword> votekeyword_list = new ArrayList<VoteKeyword>();
    	
    	try { 
	    	for  (int i = 0 ; i < votekey_list.size() ; i++){
	    		
		    	StringTokenizer Keyword_token = new StringTokenizer(votekey_list.get(i).getKeyword(), ";");
		    	
		    	log.info("키워드 분리전 " + votekey_list.get(i).getKeyword());
		    	log.info("키워드 카운트 " + "[" + votekey_list.get(i).getItem() +  "] : " + Keyword_token.countTokens());
		    	log.info("키워드 mt subject " + "[" + votekey_list.get(i).getSujebct() +  "] : " + Keyword_token.countTokens());
		    	log.info("키워드 mt msg " + "[" + votekey_list.get(i).getMt_msg() +  "] : " + Keyword_token.countTokens());
		    
		    	//Keyword_token.hasMoreTokens();
		    	//for (int j = 0 ; j < Keyword_token.countTokens() ; j++)  {
		    	while(Keyword_token.hasMoreTokens()) {
		    		VoteKeyword votekeyword = new VoteKeyword();
		    		
		    		votekeyword.setNum(votekey_list.get(i).getNum());
		    		votekeyword.setItem(votekey_list.get(i).getItem());
		    		
		    		votekeyword.setSujebct(votekey_list.get(i).getSujebct());
		    		votekeyword.setMt_msg(votekey_list.get(i).getMt_msg());
		    		
		    		votekeyword.setKeyword(Keyword_token.nextToken());
		    		
		    		log.info("keyword 분리 " + votekeyword.toString());
		    		
		    		votekeyword_list.add(votekeyword);
		    	}
	    	}	
    	} catch(Exception e) {
    		log.error(votekey_list.toString(), e.getStackTrace(), e);
    	}

    	return votekeyword_list;
    }
    
    class LengthComparator implements Comparator<VoteKeyword> {
    	
    	@Override
    	public int compare(VoteKeyword first, VoteKeyword second) {
    		
    		int firstLength = first.getKeyword().length();
    		int secondLength = second.getKeyword().length();
    		
    		// Order by descending 
    		if (firstLength > secondLength) {
    			return -1;
    		} else if (firstLength < secondLength) {
    			return 1;
    		} else {
    			return 0;
    		}
    	}
    }

    public List<VoteData>  proc_vote(VoteMsg vote_info, List<ProgramMsg> program_msgs) {
    	List<VoteData> votedata_list = new ArrayList<VoteData>();

		try { 
			for (int i = 0 ; i < program_msgs.size(); i++) {
				
		    	VoteData votedata = new VoteData();
		    	int vote_num = 0;
	        
				// pgm_key 비교
		    	if(vote_info.getPgm_key() != program_msgs.get(i).getPgm_key() || program_msgs.get(i).getPgm_key() == 0) {
		    		log.info("프로그램 키 상이 : " + program_msgs.get(i).toString());
		    		continue;
		    	}
		    	
		    	// MMO HTML 태그 제거 
		    	if(program_msgs.get(i).getMsg_type() == "MMO") {
		    		program_msgs.get(i).setMsg_data(removeTag(program_msgs.get(i).getMsg_data()));
		    	}
				
		        // 투표시간 비교
                if (vote_info.getVote_stm().compareTo(program_msgs.get(i).getMsg_date()) <= 0
                        && vote_info.getVote_etm().compareTo(program_msgs.get(i).getMsg_date()) >= 0) {
                	
    		        // 키워드 체크 (중복 무효포함 ) uppercase 해서 비교 
                	// 1 이면 중복 불가능 
               		vote_num = check_keyword(vote_info.getVote_keyword_dup(), program_msgs.get(i).getMsg_data().toUpperCase() , program_msgs.get(i).getMsg_userid());
                	
                	// 1인 다중 체크  / 1인 1투표시 redis 이용 체크 (redis 접속 실패시 db 이용체크 )
                	if(jedis != null) {
                		
                		String redis_str; 
                		if(vote_num > 0 && (vote_info.getVote_user_dup() == 1 ||  vote_info.getVote_user_dup() == 2) ) {
                			log.info("사용자 체크 REDIS 이용  ");
                			
	                		if(vote_info.getVote_user_dup() == 2 )
	                			redis_str = program_msgs.get(i).getMsg_num() + vote_info.getVote_key() + program_msgs.get(i).getMsg_userid();
	                		else 
	                			redis_str = program_msgs.get(i).getMsg_num() + vote_info.getVote_key() + program_msgs.get(i).getMsg_userid() + vote_num;
	                		
	                		log.info("Redis Check String : " + redis_str);
	                		
	                		if(jedis.get(redis_str) == null) {
	                			jedis.set(redis_str, "VOTE");
	                		}
	                		else {
	                			vote_num = -1;
	                			log.info("1인 다중 / 1인 1투표  중복 !!" + redis_str);
	                		}
                		}
                	}
                	else {
                		
                		// DB 체크 
                		if(vote_num > 0 && (vote_info.getVote_user_dup() == 1 ||  vote_info.getVote_user_dup() ==2) ) {
                			log.info("사용자 체크 DB 이용  ");
                			
                			program_msgs.get(i).setVote_key(vote_info.getVote_key());
                			program_msgs.get(i).setVote_num(vote_num);
                            
	                		int Count = VoteUtils.getVoteUserDupCheck(vote_info.getVote_user_dup() , program_msgs.get(i));
	                		if(Count > 0) {
	                			vote_num = -1;
	                			log.info("1인 다중 / 1인 1투표  중복 !!" + Count);
	                		}
                		}
                	}
                	
                	if(vote_num > 0) {
                		log.info("투표가능 사용자 : " + program_msgs.get(i).toString());
                		ReplyMt temp_replymt = new ReplyMt();
                		
                		votedata.setVote_key(vote_info.getVote_key());
                		votedata.setVote_user(program_msgs.get(i).getMsg_userid());
                		votedata.setVote_num(vote_num);
                		
                		// mt setting
                		
           				// callback 
                		votedata.setMsg_callback(program_msgs.get(i).getMsg_num() + program_msgs.get(i).getMsg_emo());
        				
        				// ref_key (br_key) 
        				temp_replymt =  VoteUtils.selectBillingCode(program_msgs.get(i).getPgm_key());
        				votedata.setCh_key(temp_replymt.getCh_key());
        				votedata.setRef_key(temp_replymt.getBilling_code());
        				
        				// msg_data set
                		votedata.setSubject(votemsg_list.get(vote_num-1).getSujebct());
                		votedata.setMt_msg(votemsg_list.get(vote_num-1).getMt_msg());
                        
                        votedata_list.add(votedata);
                	}
 
                }
                else if (vote_info.getVote_etm().compareTo(program_msgs.get(i).getMsg_date()) < 0) {
                	// 투표 종료 처리
                	log.info("인입 메시지가 투표 시간을 초과 하였습니다. 투표 종료 :  " + program_msgs.get(i).toString());
                	
                	// Update Status 
                	// 투표 status 3로 변경
                    VoteUtils.updateMacStatus(3, vote_info);
                	
                	// 종료 처리 
                    Stop_Vote(vote_info);
                }
                else {
                	log.info("인입 메시지가 투표 시간이 아닙니다. :  " + program_msgs.get(i).toString());
                }
			}
			
		} catch(Exception e) {
			log.error(program_msgs.toString(), e.getStackTrace(), e);
		}
		
		return votedata_list;   	
    }
    
    public String removeTag(String html) throws Exception {
    	return html.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
    }
    
    public int check_keyword(int Dup, String msg_data, String msg_userid)  {
    	int num = 0; 

    	// 무효화 
    	try { 
	    	if(Dup == 1 ) { 
		    	for(int i = 0 ; i < votekeywords.size(); i++) {
		    		if ( msg_data.contains(votekeywords.get(i).getKeyword()) ) {
		    			
		                if(num == 0) {
		                    num = votekeywords.get(i).getNum();
		                } else if(num != votekeywords.get(i).getNum()){
		                	log.info("키워드가 2개가 들어 있습니다.  :  " + msg_data );
		                    return -2;
		                }
		    		}
		    	}
	    	}
	    	else {
		    	for(int i = 0 ; i < votekeywords.size(); i++) {
		    		if ( msg_data.contains(votekeywords.get(i).getKeyword()) ) {	    			
		    			return  votekeywords.get(i).getNum();
		    		}
		    	}
	    	}
    	} catch(Exception e) {
			log.error( msg_userid, e.getStackTrace(), e);
		}
    	
    	return num;
    }
    
    public void Stop_Vote(VoteMsg vote)  {
 	
    	try { 
    		
    		log.info( "투표를 종료 합니다 " + vote.toString());
    		shutdown();
	
    	} catch(Exception e) {
			log.error( vote.toString(), e.getStackTrace(), e);
		}

    }

    private class ShutdownHook extends Thread {
        private Thread target;
        
        public ShutdownHook(Thread target, String name) {
            super(name);
            this.target = target;
        }
        
        public void run() {
            shutdown();
            
            try {
                //target 쓰레드가 종료될 때 까지 기다린다.
                target.join();                                
            } catch (InterruptedException e) {
                log.error("sth wrong in shutdown process", e);
            }
        }
    }
}
