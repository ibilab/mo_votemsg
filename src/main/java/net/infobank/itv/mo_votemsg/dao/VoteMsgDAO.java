package net.infobank.itv.mo_votemsg.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.ProgramMsg;
import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_votemsg.model.VoteMsg;
import net.infobank.itv.mo_votemsg.model.ReplyMt;
import net.infobank.itv.mo_votemsg.model.VoteData;

public class VoteMsgDAO {
    private static Logger log = LoggerFactory.getLogger(VoteMsgDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public VoteMsgDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

	public List<VoteMsg> selectVoteMsgList(VoteMsg vote) {
		List<VoteMsg> list = new ArrayList<VoteMsg>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			
			if(vote.getVote_server_id() == 1) {
				vote.setStr_vote_server_id("mac_status");
			}
			else { 
				vote.setStr_vote_server_id("mac_status_" + vote.getVote_server_id());
			}
			
			list = session.selectList("Vote.selectVoteMsgList", vote);
        } catch(Exception e)  {
            log.error("Vote.selectVoteMsgList : " , e);			
		} finally {
			session.close();
		}

		return list;
	}
	

	public int  getVoteUserDupCheck(int dup , ProgramMsg msg) {
			
		int ret = 0;
		SqlSession session = sqlSessionFactory.openSession();
		try {
			
			if(dup == 2) {
				
				ret = session.selectOne("Vote.getVoteUserOneDupCheck", msg);
			}
			else {
				
				ret = session.selectOne("Vote.getVoteUserMultiDupCheck", msg);
			}
			
        } catch(Exception e)  {
            log.error("Vote.getVoteUserDupCheck : " , e);			
		} finally {
			session.close();
		}

		return ret;
	}
		
	
		
	public void  updateMacStatus(VoteMsg vote) {
		
		 SqlSession session = sqlSessionFactory.openSession();
		 
		try {
			
			session.update("Vote.updateMacStatus", vote);
	    } catch(Exception e)  {
	          log.error("Vote.updateMacStatus : " , e);			
		} finally {
			session.commit();
	        session.close();
		}
		
	}
	
	
	
	public int getMacStatus(VoteMsg vote) {

		int status = 0;
		SqlSession session = sqlSessionFactory.openSession();

		try {
			status = session.selectOne("Vote.getMacStatus", vote);
			
        } catch(Exception e)  {
            log.error("Vote.getMacStatus : " , e);			
		} finally {
			session.close();
		}

		return status;
	}

	public ReplyMt selectBillingCode(int pgm_key) {
		ReplyMt replymt = null;
		HashMap<String,String> map = new HashMap<String,String>();
		
		Integer to_pgm_key = pgm_key;
		map.put("pgm_key", to_pgm_key.toString());
		
		SqlSession session = sqlSessionFactory.openSession();

		try {
			replymt = session.selectOne("VoteMsgData.selectBillingCode", map);
			//log.info(replymt.toString());
			
		} catch(Exception e)  {
		    log.error("VoteMsgData.selectBillingCode : " , e);
		} finally {
			session.close();
		}
		return replymt;
	}
    
    public int mtBulkInsert_2(List<VoteData> list) {
        int id = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
        	
        	log.info(list.toString());
        
        	for(int i = 0; i < list.size() ; i++) {
        		
        		/*
        		if(list.get(i).getMsg_data().length() < 81)
        			id = session.insert("ReplyMt.mtBulkInsert_2_sms", list.get(i));
        		else
        		*/ 
                id = session.insert("VoteMsgData.mtBulkInsert_2_lms", list.get(i));
        	}
            session.commit();
        } catch(Exception e)  {
            log.error("VoteMsgData.bulkinsert_2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return id;
    }

}
